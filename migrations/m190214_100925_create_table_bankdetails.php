<?php

use yii\db\Migration;

/**
 * Class m190214_100925_create_table_bankdetails
 */
class m190214_100925_create_table_bankdetails extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_100925_create_table_bankdetails cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_100925_create_table_bankdetails cannot be reverted.\n";

        return false;
    }
    */
}
