<?php

use yii\db\Migration;

/**
 * Class m190214_100939_create_table_jobs
 */
class m190214_100939_create_table_jobs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_100939_create_table_jobs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_100939_create_table_jobs cannot be reverted.\n";

        return false;
    }
    */
}
