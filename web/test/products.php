	<?php require_once('menu.php');?>

			<div class="european-products-wrap">

				<!-- 1 СТРОКА -->
				<div class="european-products-item-wrap">
					<div class="european-products-item borrowers-insurance wow slideInDown">
						<a href="product-ensure.php">Страхование заемщиков</a>
					</div>

					<div class="european-products-item corporate-insurance wow slideInRight">
						<a href="product-corp.php">Корпоративное страхование</a>
					</div>
				</div>
				<!-- END 1 СТРОКА -->

				

				<!-- 2 СТРОКА -->
				<div class="european-products-item-wrap ">
					<div class="european-products-item family-protect wow slideInLeft">
						<a href="product-family.php">Защита семьи</a>
					</div>

					<div class="european-products-item savings-insurance wow slideInUp">
						<a href="product-capital.php">Накопительное страхование</a>
					</div>
				</div>
				<!-- END 2 СТРОКА -->

			</div>
		</div>
	</div>

	<?php require_once('footer.php');?>