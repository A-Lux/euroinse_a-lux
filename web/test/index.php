	<?php require_once('menu.php');?>

		<div class="european-content">
			
			<!-- 1 СТРОКА -->
			<div class="european-item-wrap">
				<div class="european-insurance-case european-item">
					<a href="ensure.php">Страховой случай</a>
				</div>

				<div class="european-change-insurance european-item">
					<a href="change-ensure.php">Изменение договора страхования</a>
				</div>

				<div class="european-products european-item">
					<a href="products.php">Продукты</a>
				</div>
			</div>
			
			<!-- END 1 СТРОКА -->


			<!-- 2 СТРОКА -->
			<div class="european-item-wrap">
				<div class="european-pay european-item">
					<a href="pay.php">Оплата</a>
				</div>

				<div class="european-client-support european-item">
					<a href="support.php">Клиентская поддержка</a>
				</div>

				<div class="european-contacts european-item">
					<a href="support.php">Контакты</a>
				</div>
			</div>
			<!-- END 2 СТРОКА -->

		</div>

	</div>

	<?php require_once('footer.php');?>