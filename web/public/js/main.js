(function($) {
    $('.modal-big1').hide();
    $('.big1').on('click', function(e) {
        e.preventDefault();
        $('.modal-big1').scrollTop(0);
        $('.modal-big1').slideToggle(500);
    })

    $('.close1').on('click', function() {
        $('.modal-big1').hide(500);
    })
    $('.modal-big2').hide();
    $('.big2').on('click', function(e) {
        e.preventDefault();
        $('.modal-big2').slideToggle(500);
    })

    $('.close2').on('click', function() {
        $('.modal-big2').hide(500);
    })
    new WOW().init();

    $(document).ready(function(){
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('+7(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
    });

   /* OffCanvas Menu
    * ------------------------------------------------------ */
    var clOffCanvas = function() {

        var menuTrigger     = $('.header-menu-toggle'),
            nav             = $('.header-nav'),
            closeButton     = nav.find('.header-nav__close'),
            siteBody        = $('body'),
            mainContents    = $('section, footer');

        // open-close menu by clicking on the menu icon
        menuTrigger.on('click', function(e){
            e.preventDefault();
            // menuTrigger.toggleClass('is-clicked');
            siteBody.toggleClass('menu-is-open');
        });

        // close menu by clicking the close button
        closeButton.on('click', function(e){
            e.preventDefault();
            menuTrigger.trigger('click');
        });

        // close menu clicking outside the menu itself
        siteBody.on('click', function(e){
            if( !$(e.target).is('.header-nav, .header-nav__content, .header-menu-toggle, .header-menu-toggle span') ) {
                // menuTrigger.removeClass('is-clicked');
                siteBody.removeClass('menu-is-open');
            }
        });

    };

   /* Initialize
    * ------------------------------------------------------ */
    (function ssInit() {
        clOffCanvas();
    })();


})(jQuery);



$(function(){

    // BURGER MENU
    $('.hamburger-item').on('click', function(event) {
        event.preventDefault;
        $(this).toggleClass('hamburger-active');
        $('.hamburger-menu').toggleClass('hamburger-menu-opened');
        $('.menu-wrap').toggleClass('menu-wrap-opened')
    })
    // END BURGER MENU


    // ЯКОРЬ ВВЕРХ
    $(".arrow-up a").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
      });
    // END ЯКОРЬ ВВЕРХ

    // CLOSE POPUP PAY
    $('.european-pay-close').on('click', function(e) {
        e.preventDefault()
        $(this).parent().parent().hide()
    })
    // END CLOSE POPUP PAY

})
$(document).ready(function(){

    $(".modal-window1").hide();
    $(".one").click(function(){
        $(".modal-window1").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window1");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });

    $(".modal-window2").hide();
    $(".two").click(function(){
        $(".modal-window2").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window2");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });

    $(".modal-window3").hide();
    $(".three").click(function(){
        $(".modal-window3").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window3");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
    $(".modal-window4").hide();
    $(".four").click(function(){
        $(".modal-window4").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window4");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
    $(".modal-window5").hide();
    $(".five").click(function(){
        $(".modal-window5").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window5");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
    $(".modal-window6").hide();
    $(".six").click(function(){
        $(".modal-window6").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window6");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
    $(".modal-window7").hide();
    $(".seven").click(function(){
        $(".modal-window7").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window7");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
    $(".modal-window8").hide();
    $(".eight").click(function(){
        $(".modal-window8").show();
    });
    $(document).mouseup(function(e)
    {
        var container = $(".modal-window8");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });

});


