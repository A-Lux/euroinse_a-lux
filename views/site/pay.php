<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Pay';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="https://widget.cloudpayments.kz/bundles/cloudpayments"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<div class="european-pay-content">

    <!-- Оплата прошла успешно -->
    <div class="european-pay-successfully-wrap">
        <div class="european-pay-successfully">
            <img src="/public/images/pay-successfully-title.png" alt="Оплата прошла успешно">
            <h5>Спасибо! Оплата прошла успешно.</h5>
            <a href="#" class="european-pay-close">Закрыть</a>
        </div>
    </div>
    <!-- End Оплата прошла успешно -->

    <!-- Оплата ошибка -->
    <div class="european-pay-mistake-wrap" id="pay-mistake">
        <div class="european-pay-mistake">
            <img src="/public/images/pay-mistake-title.png" alt="Оплата прошла успешно">
            <h4>Ошибка</h4>
            <h5>Свяжитесь с нашим консультаном</h6>

                <div class="european-pay-mistake-phone">
                    <img src="/public/images/european-mistake-phone.png" alt="Телефон">
                    <h3>+7 (727) 244 36 80
            </h5>
        </div>

        <a href="#" class="european-pay-close">Закрыть</a>
    </div>
</div>
<!-- End Оплата ошибка -->


<div class="european-pay-title wow slideInLeft">
    <h4><?= $pay->title ?></h4>
    <h3><?= $pay->subtitle ?></h3>
</div>

<div class="european-pay-form-wrap">
    <form class="european-pay-form" action="" id="pay-form">
		<label class="wow slideInLeft" for="PayForm[number_contract]" style="color: #ccc;"><?= $pay->pholder1 ?></label>
        <input class="wow slideInLeft" id="feedContract" name="PayForm[number_contract]" data-wow-delay="0.1s"
               type="text"required>
		<label class="wow slideInLeft" for="PayForm[iin]" style="color: #ccc;"><?= $pay->pholder2 ?></label>
        <input class="wow slideInLeft" id="feedIin" name="PayForm[iin]" data-wow-delay="0.2s" type="text" required>
        <!--        <input class="wow slideInLeft" data-wow-delay="0.3s" type="text" placeholder="-->
        <? //=$pay->pholder3?><!--">-->
		<label class="wow slideInLeft" for="PayForm[sum]" style="color: #ccc;"><?= $pay->pholder4 ?></label>
        <input class="feedSum wow slideInLeft" id="feedSum" name="PayForm[sum]" data-wow-delay="0.4s" type="number" required>
		<label class="wow slideInLeft" for="PayForm[phone]" style="color: #ccc;"><?= $pay->pholder5 ?></label>
        <input class="wow slideInLeft" id="feedPhone" name="PayForm[phone]" data-wow-delay="0.45s" type="text" required>

        <div class="invest-agree-checkbox wow fadeInUp">
            <input id="agree-pers" name="check" type="checkbox" required>
            <label for="agree-pers"><a href="/site/pay-information"><?= $pay->agree ?></a></label>
        </div>
        <button type="button" class="wow fadeInUp send-pay-form" id="checkout"><?= $pay->buttonName ?></button>
    </form>


</div>


<script>
    $('#feedPhone').inputmask("+7(999) 999-9999");
    $('#feedIin').inputmask("999999999999");
    $('#feedContract').inputmask("9999999999");
</script>
<script>

    $('body').on('click', ".send-pay-form", function (e) {
        var formdata = $(this).closest('form').serialize();

        iinStatus = true;
        var iin = $("#feedIin").val();
        for (var i = 0; i < iin.length; i++) {
            if (iin.charAt(i) == '_') {
                iinStatus = false;
            }
        }

        phoneStatus = true;
        var phone = $("#feedPhone").val();

        for (var j = 0; j < phone.length; j++) {
            if (phone.charAt(j) == '_') {
                phoneStatus = false;
            }
        }
        var sum = $("#feedSum").val();

        contractStatus = true;
        var contract = $("#feedContract").val();
        for (var k = 0; k < contract.length; k++) {
            if (contract.charAt(k) == '_') {
                contractStatus = false;
            }
        }

        if (!contractStatus || contract == 0) {
            swal('Заполните номер договора');
        } else if (!iinStatus || iin == 0) {
            swal('ИИН должен состоять из 12 цифров');
        } else if (!sum) {
            swal('Заполните поле сумма');
        } else if (!phoneStatus || phone == 0) {
            swal('Телефон должен состоять из 10 цифров');
        } else {
            $.ajax({
                type: "GET",
                url: "/site/pay-form",
                data: formdata,
                success: function (data) {
                    if (data == 1) {

                        if ($("#agree-pers").is(":checked")) {
                            var widget = new cp.CloudPayments();

                            var amount = parseFloat($('#feedSum').val());

                            var accountId = ($('#feedIin').val()+', '+$('#feedPhone').val());

                            var invoiceId = $('#feedContract').val();

                            widget.charge({ // options
                                    publicId: 'pk_3aa0016b4f8da8246c3d6f66e010f', //id из личного кабинета
                                    description: 'Оплата', //назначение
                                    amount: amount, //сумма
                                    currency: 'KZT', //валюта
                                    invoiceId : invoiceId, //номер заказа  (необязательно)
                                    accountId: accountId, //идентификатор плательщика (обязательно для создания подписки)
                                    email: '',
                                    data: formdata
                                },
                                function (options) { // success
                                    //действие при успешной оплате

                                    $.ajax({
                                        type: "GET",
                                        url: "/site/pay-form-db",
                                        data: formdata,
                                        success: function (data) {
                                            if (data == 1) {
                                                setTimeout(function () {
                                                    window.location.href = "/site/pay";
                                                }, 1500);
                                            } else {
                                                swal('eeeerrrrorr', data, 'error');
                                            }
                                        },
                                        error: function () {
                                            swal('Error', 'error');
                                        }
                                    });

                                },
                                function (reason, options) { // fail
                                    //действие при неуспешной оплате
                                    $.ajax({
                                        type: "GET",
                                        url: "/site/pay-db-false",
                                        data: formdata,
                                        success: function (data) {
                                            if (data == 1) {

                                            } else {
                                                swal(data, 'error');
                                            }
                                        },
                                        error: function () {
                                            swal('Error', 'error');
                                        }
                                    });
                                });
                        } else {
                            swal('Чтобы продолжить установите флажок');
                        }
                    } else {
                        // $('#pay-mistake').show();
                        swal(data, 'error');
                    }
                },
                error: function () {
                    swal('Error', 'error');
                }
            });
        }
    });


</script>


</div>
</div>
</div>
