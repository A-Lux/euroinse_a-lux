<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'support';
$this->params['breadcrumbs'][] = $this->title;


if($activeBlock == "cont"){
    $cont = "active";$fao="" ;$glos="";$doc="";
}elseif ($activeBlock == "fao"){
    $cont = "";$fao="active" ;$glos="";$doc="";
}elseif ($activeBlock == "glos"){
    $cont = "";$fao="" ;$glos="active";$doc="";
}elseif ($activeBlock == "doc"){
    $cont = "";$fao="" ;$glos="";$doc="active";
}
?>


<div class="support-content">
    <div class="tabs wow slideInLeft">
        <ul class="nav nav-tabs ">
            <li class="nav-item">
                <a class="nav-link <?=$cont?>" data-toggle="tab" href="#contacts"><?=$title[0]["title"]?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$fao?>" data-toggle="tab" href="#head"><?=$title[1]["title"]?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$glos?>" data-toggle="tab" href="#licence"><?=$title[2]["title"]?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$doc?>" data-toggle="tab" href="#bank"> <?=$title[3]["title"]?> </a>
            </li>
        </ul>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane <?=$cont?>" id="contacts">
            <div class="contacts wow slideInUp">
                <div class="contact-text">
                    <?php if($contact[0]["text1"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?><p> <a class="no-m-l" href=""><?=$contact[0]["text1"]?></a></p>
                        <?php }else {?><p> <a href=""><?=$contact[0]["text1"]?></a></p><?php }?>
                    <?php endif;?>
                    <?php if($contact[0]["telephoneNumber"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?>  <p><b>Call-центр:</b> <a href="tel: <?=$contact[0]["telephoneNumber"]?>"><?=$contact[0]["telephoneNumber"]?></a></p>
                        <?php }else {?> <p><b>Байланыс телефоны:</b> <a href="tel: <?=$contact[0]["telephoneNumber"]?>"><?=$contact[0]["telephoneNumber"]?></a></p><?php }?>
                    <?php endif;?>
                    <?php if($contact[0]["whatsapNumber"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?>  <p><b>WhatsApp:</b><a href="https://api.whatsapp.com/send?phone=+77010841028&amp;text= "]?"><?=$contact[0]["whatsapNumber"]?></a></p>
                        <?php }else { ?> <p><b>WhatsApp:</b><a href="tel: <?=$contact[0]["whatsapNumber"]?>"><?=$contact[0]["whatsapNumber"]?></a></p><?php }?>
                    <?php endif;?>
                    <?php if($contact[0]["faxNumber"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?> <p><b>Факс:</b><a href=""><?=$contact[0]["faxNumber"]?></a></p>
                        <?php }else {?><p><b>Факс:</b><a href=""><?=$contact[0]["faxNumber"]?></a></p><?php }?>
                    <?php endif;?>
                    <?php if($contact[0]["email"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?> <p><b>E-mail:</b><a href="mailto:info@euroins.kz?Subject=Hello%20again"><?=$contact[0]["email"]?></a></p>
                        <?php }else {?> <p><b>Электрондық пошта:</b><a href="mailto:info@euroins.kz?Subject=Hello%20again"><?=$contact[0]["email"]?></a></p><?php }?>
                    <?php endif;?>
                    <?php if($contact[0]["webSite"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?> <p><b>Web сайт:</b><a href=""><?=$contact[0]["webSite"]?></a></p>
                        <?php }else {?> <p><b>Web сайт:</b><a href=""><?=$contact[0]["webSite"]?></a></p><?php }?>
                    <?php endif;?>
                    <?php if($contact[0]["ugol"] != null && $contact[0]["prospect"]):?>
                        <?php if(Yii::$app->session["lang"] == ""){?>    <p><b>Адрес:</b><a href=""><?=$contact[0]["prospect"]?><br>
                                <?=$contact[0]["ugol"]?>
                            </a></p>
                        <?php }else {?>   <p><b>Мекен-жайымыз:</b><a href=""><?=$contact[0]["prospect"]?><br>
                                <?=$contact[0]["ugol"]?><?php }?>
                            </a></p>
                    <?php endif;?>
                    <?php if($contact[0]["text2"] != null):?>
                        <?php if(Yii::$app->session["lang"] == ""){?>  <p><a href=""><?=$contact[0]["text2"]?></a></p>
                        <?php }else {?>   <p><a href=""><?=$contact[0]["text2"]?></a></p><?php }?>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="tab-pane <?=$fao?>" id="head">
            <?php $m = 0;$delay=0;?>
            <?php foreach($faos as $fao):?>
            <?php $m++;$delay+=0.1;?>
                <?php if($m<5){?>
                    <div class="qa-block wow fadeInLeft" data-wow-delay="<?=$delay;?>s">
                        <div class="qa-section" href="<?="#collapseExample".$m;?>" data-toggle="collapse">
                            <?=$fao['title']?>
                            <img src="/public/images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="<?="collapseExample".$m;?>">
                            <div class="answer">
                                <p> <?=$fao['content']?></p>
                            </div>
                        </div>
                    </div>
                <?php }?>
            <?php endforeach; ?>
            <?if($m > 4):?>
            <div class="more wow fadeInUp"  data-wow-delay="0.45s">
                <p href="#collapse-all" data-toggle="collapse">Показать еще <img src="/public/images/more-ico.png" alt=""></p>
            </div>
            <?php endif;?>
            <div class="collapse" id="collapse-all">
                <?php $m = 0;?>
                <?php foreach($faos as $fao):?>
                    <?php $m++;?>
                    <?php if($m>4){?>
                        <div class="qa-block">
                            <div class="qa-section" href="<?="#collapseExample".$m;?>" data-toggle="collapse">
                                <?=$fao['title']?>
                                <img src="/public/images/plus-ico.png" alt="">
                            </div>
                            <div class="collapse" id="<?="collapseExample".$m;?>">
                                <div class="answer">
                                    <p> <?=$fao['content']?></p>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="tab-pane <?=$glos?>" id="licence">
            <?php foreach($glossaries as $glossary):?>
                <div class="qa-block wow fadeInRight">
                    <div class="qa-section" href="#lib<?=$glossary["id"]?>" data-toggle="collapse">
                        <?=$glossary['title']?>
                        <img src="/public/images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="lib<?=$glossary["id"]?>">
                        <div class="lib-text">
                            <?=$glossary["content"]?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="tab-pane <?=$doc?>" id="bank">
            <?php $n = 0; ?>
            <?php foreach ($documents as $document): ?>
                <?php $n++; ?>
                <div class="qa-block">
                    <div class="qa-section wow slideInLeft" href="#collapse<?=$n;?>" data-toggle="collapse">
                        <?=$document->title?>
                        <img src="/public/images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapse<?=$n;?>">
                        <div class="answer-split">
                            <?php $types = $document->documentItems ?>

                            <?php foreach ($types as $type):?>
                                <p><a href="<?=$type->getFile()?>" target = "_blank"><?=$type->name?></a></p>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
           
            <?php endforeach; ?>

            <!-- <?if($n > 4):?>
            <div class="more wow fadeInUp" data-wow-delay="0.5s">
                <p href="#collapse-all2" data-toggle="collapse">Показать еще <img class="img" src="/public/images/more-ico.png" alt=""></p>
            </div>
            <?php endif;?>

            <div class="collapse" id="collapse-all2">
                <?php $n = 1;?>
                <?php foreach ($documents as $document): ?>
                    <?php $n++?>
                    <?php if($n>5){?>
                        <div class="qa-block">
                            <div class="qa-section" href="#collapse<?=$n;?>" data-toggle="collapse">
                                <?=$document->title?>
                                <img src="/public/images/plus-ico.png" alt="">
                            </div>
                            <div class="collapse" id="collapse<?=$n;?>">
                                <div class="answer-split">
                                    <?php $types = $document->documentItems ?>

                                    <?php foreach ($types as $type):?>
                                        <p><?=$type->name?><a href="<?=$type->getFile()?>">Скачать PDF</a></p>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                <?php endforeach; ?> -->
            <!-- </div> -->
        </div>
    </div>
</div>
<!-- End Оплата ошибка -->

</div>
</div>
</div>
