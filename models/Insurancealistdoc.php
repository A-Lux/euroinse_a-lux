<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurancealistdoc".
 *
 * @property int $id
 * @property string $file
 * @property int $insurancealist_id
 */
class Insurancealistdoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurancealistdoc'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insurancealist_id','file'], 'required'],
            [['insurancealist_id'], 'integer'],
            [['file'], 'file', 'extensions' => 'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'PDF Файл',
            'insurancealist_id' => 'Категории',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Insurancealist::className(), ['id' => 'insurancealist_id']);
    }

    public function getDocumentName(){
        return (isset($this->document))? $this->document->name:'Не задан';
    }

    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }
}
