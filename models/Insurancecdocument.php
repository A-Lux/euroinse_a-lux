<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurancecdocument".
 *
 * @property int $id
 * @property string $name
 * @property string $file
 * @property int $insuranceccontent_id
 */
class Insurancecdocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurancecdocument'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'insuranceccontent_id'], 'required'],
            [['insuranceccontent_id'], 'integer'],
            [['name', 'file'], 'string', 'max' => 255],
            [['file'],'file','extensions' => 'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'file' => 'PDF Файл',
            'insuranceccontent_id' => ' Категории',
        ];
    }

    public function getContent()
    {
        return $this->hasOne(Insuranceccontent::className(), ['id' => 'insuranceccontent_id']);
    }

    public function getContentName(){
        return (isset($this->content))? $this->content->name:'Не задан';
    }

    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }



}
