<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_fao".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductFao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_fao'.Yii::$app->session["lang"];;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'product_id'], 'required'],
            [['content'], 'string'],
            [['product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Содержание',
            'product_id' => 'Продукт',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }


    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProductName(){
        return (isset($this->product))? $this->product->title : 'Не задано';
    }
}
