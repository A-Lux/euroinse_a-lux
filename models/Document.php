<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string $title
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Названия',
        ];
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Document::find()->all(),'id','title');
    }

    public function getDocumentItems(){
        return $this->hasMany(Typesofdocument::className(), ['document_id' => 'id']);
    }



}
