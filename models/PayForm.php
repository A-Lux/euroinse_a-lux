<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pay_form".
 *
 * @property int $id
 * @property string $number_contract
 * @property int $iin
 * @property string $sum
 * @property string $phone
 * @property int $status
 */
class PayForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_contract', 'iin', 'phone'], 'required'],
            [[ 'status'], 'integer'],
            [['number_contract', 'iin', 'sum', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number_contract' => 'Номер договора',
            'iin' => 'ИИН',
            'sum' => 'Сумма',
            'phone' => 'Номер телефона',
            'status' => 'Статус',
        ];
    }
}
