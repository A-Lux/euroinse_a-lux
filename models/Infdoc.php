<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "infdoc".
 *
 * @property int $id
 * @property string $text
 * @property string $file
 */
class Infdoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'infdoc'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string', 'max' => 255],
            [['file'],'file','extensions'=>'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'file' => 'PDF Файл',
        ];
    }


    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }
}
