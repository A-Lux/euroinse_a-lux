<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BankdetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Банковские реквизиты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bankdetails-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать банковские реквизиты', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'subtitle',
            //'rnn',
            //'bin',
            //'iik',
            //'bank',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
