<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsurancecSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Виды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancec-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            ['class' => 'yii\grid\ActionColumn','template' => '{update} {view}'],
        ],
    ]); ?>
</div>
