<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insurancec */

$this->title = 'Редактировании виды: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Insurancecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="insurancec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
