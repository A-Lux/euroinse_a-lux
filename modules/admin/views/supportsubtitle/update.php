<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Supportsubtitle */

$this->title = 'Редактирование подкатегории поддержки: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Supportsubtitles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="supportsubtitle-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
