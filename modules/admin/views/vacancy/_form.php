<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vacancy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?php
    $emp = array('Полная занятость'=>'Полная занятость','Частичная занятость'=>'Частичная занятость','Стажировка'=>'Стажировка',
        'Проектная/Временная работа'=>'Проектная/Временная работа','Волонтерство'=>'Волонтерство');
    ?>

    <?= $form->field($model, 'employment')->dropDownList($emp) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'date')-> ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
