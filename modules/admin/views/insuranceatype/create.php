<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insuranceatype */

$this->title = 'Создании подкатегории списка';
$this->params['breadcrumbs'][] = ['label' => 'Insuranceatypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insuranceatype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
