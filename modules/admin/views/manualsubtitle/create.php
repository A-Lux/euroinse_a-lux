<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manualsubtitle */

$this->title = 'Создании подкатегории руководсто';
$this->params['breadcrumbs'][] = ['label' => 'Manualsubtitles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manualsubtitle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
