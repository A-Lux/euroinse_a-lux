<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fao */

$this->title = 'Создание вопросы и ответы';
$this->params['breadcrumbs'][] = ['label' => 'Faos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
