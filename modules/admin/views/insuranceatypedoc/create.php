<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insuranceatypedoc */

$this->title = 'Создании документа для подкатегорий';
$this->params['breadcrumbs'][] = ['label' => 'Insuranceatypedocs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insuranceatypedoc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
