<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Infdoc */

$this->title = 'Создать документы';
$this->params['breadcrumbs'][] = ['label' => 'Infdocs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infdoc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
