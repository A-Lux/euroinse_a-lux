<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contracta */

$this->title = 'Создание страхование заемщиков';
$this->params['breadcrumbs'][] = ['label' => 'Contractas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
