<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSubcategoryFaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Выпадающий список категорий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ensurefao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'product_subcategory_id',
                'value' => function($model){
                    return $model->productSubcategoryTitle;
                },
                'filter' => \app\models\ProductSubcategory::getList()
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
