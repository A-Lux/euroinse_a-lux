<?php

namespace app\modules\admin\controllers;

use app\models\FileUpload;
use app\models\ImageUpload;
use Yii;
use app\models\License;
use app\models\LicenseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * LicenseController implements the CRUD actions for License model.
 */
class LicenseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all License models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LicenseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single License model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new License model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new License();
        $fileupload = new FileUpload();
        $imageUpload = new ImageUpload();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $image = UploadedFile::getInstance($model, 'image');
            if ($file == null && $image == null) {
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }elseif ($file == null){
                if ($model->saveImage($imageUpload->uploadFile($image, $model->image)) && $model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }elseif ($image == null) {
                $file->saveAs($fileupload->getFolder() . $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                if ($model->saveImage($imageUpload->uploadFile($image, $model->image)) && $model->save(false)) {
                    $file->saveAs($fileupload->getFolder() . $file->baseName . '.' . $file->extension);
                    $model->file = $file->baseName . '.' . $file->extension;
                    $model->save(false);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);


    }

    /**
     * Updates an existing License model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $fileUpload = new FileUpload();
        $imageUpload = new ImageUpload();
        $oldFileName = $model->file;
        $oldImageName = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $image = UploadedFile::getInstance($model, 'image');
            if($file == null && $image == null){
                $model->file = $oldFileName;
                $model->image = $oldImageName;
                $model->save();
            }elseif ($image == null){
                $model->image = $oldImageName;
                $file->saveAs($fileUpload->getFolder() . $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;
                $model->save(false);

                if(!($oldFileName == null)){
                    unlink($fileUpload->getFolder() . $oldFileName);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }elseif ($file == null){
                $model->file = $oldFileName;
                if($model->saveImage($imageUpload->uploadFile($image, $model->image)) && $model->save(false)) {
                    if(!($oldImageName == null)){
                        unlink($imageUpload->getFolder() . $oldFileName);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing License model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the License model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return License the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = License::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
