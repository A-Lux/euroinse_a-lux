<?php


namespace app\controllers;


use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductFao;
use app\models\ProductSubcategoryFao;
use yii\web\NotFoundHttpException;

class ProductController extends SiteController
{
    public function actionIndex(){
        $products = Product::find()->all();
        return $this->render('index', compact('products'));
    }

    public function actionView($id){
        $product = Product::findOne($id);
        if($product){

            $products = Product::find()->all();
            $categories = ProductCategory::find()->where(['product_id' => $id])->with('subcategories')->all();

            return $this->render('view', compact('products', 'categories', 'fao', 'product'));
        }else{
            throw new NotFoundHttpException();
        }

    }
}