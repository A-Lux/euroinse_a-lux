<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       "public/css/bootstrap.css",
       "public/css/owl.carousel.min.css",
       "public/css/owl.theme.default.min.css",
       "public/css/animate.css",
       "public/css/style.css",
       "https://fonts.googleapis.com/css?family=Lobster",
       "public/css/smoothbox.css",
    ];
    public $js = [
       "public/js/jquery-3.2.1.min.js",
       "public/js/wow.min.js",
       "public/js/jquery.mask.min.js",
       "public/js/owl.carousel.min.js",
       "public/js/main.js",
       "http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js",
       "public/js/smoothbox.min.js",
       "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
       "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
    ];
    public $depends = [

    ];
}
